---
layout: markdown_page
title: GitLab Hosted
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## GitHost is shutting down on June 1st, 2019

On June 1st 2019, GitLab will shut down GitHost, our hosted single-tenant offering.

We are currently not accepting new customers. GitHost will continue to operate as normal for existing customers until June 1st 2019. Any existing individual agreements that extend beyond this date will be honored.

### Why is GitHost shutting down?

Instead of maintaining both a shared (GitLab.com) and single-tenant managed GitLab service, we'd like to focus on GitLab.com.

### As an existing GitHost user, what options do I have?

GitHost will continue to operate as normal until June 1st 2019. If you'd like to maintain your GitLab instance beyond that date, you can:

* Migrate your instance to GitLab.com, our managed and shared offering.
  * More information on moving from to GitLab.com can be [found here](https://docs.gitlab.com/ee/user/project/import/).
* Self-host a GitLab instance.

Some 3rd party vendors also offer a single-tenant managed offering, such as [GitLabHost](https://gitlabhost.com/).

Please note that your current `*.githost.io` subdomain is **not** able to be migrated along with your data, regardless of the solution you choose.

### How do I export my instance data?

Moving instances is currently supported by the [project import/export](https://docs.gitlab.com/ee/user/project/settings/import_export.html) feature. Projects can be individually exported via the web interface. An [API](https://docs.gitlab.com/ee/api/project_import_export.html) is also available.

### If I'm using GitLab CE on GitHost, do I need to get a paid plan on GitLab.com?

No, you can migrate to GitLab.com using the Free tier. You can always upgrade later if you decide you'd like the additional features of a paid tier.

Public projects on GitLab.com also benefit from Gold features for free.

### Where can I learn more about GitLab.com?

See [GitLab.com's pricing page](/pricing/#gitlab-com) for more information about GitLab.com.

Please note that some features are not supported on GitLab.com. Please see the related question in the [FAQ section](/pricing/#gitlab-com) for more detail.

## Other FAQs for Current Customers of GitHost

### What is GitLab Hosted a.k.a. GitHost?

[GitHost](https://githost.io) is a single-tenant solution that provides GitLab installations and CI runners as
a managed service. GitLab Inc. is responsible for installing, updating, hosting, and
backing up customers own private and secure GitLab instances.

### Why should I use GitHost?

* GitHost removes the system administration overhead of having to manage
  hardware, infrastructure, upgrades and back-ups, without compromising security
* It allows customers to concentrate on their core business of delivering
  applications to end users
* It facilitates the use of GitLab for organizations who do not have experience in
  GitLab, Linux or MySQL/PostgreSQL

### What is different about GitHost than other hosted Git solutions?

* Unlike GitHub.com, Bitbucket.org and GitLab.com, GitHost is a single-tenant
  solution which means your IP does not share the underlying infrastructure with
  other customers
* GitHost can integrate with your Active Directory or LDAP server for better
  authentication, as well as SAML, Crowd and all the OmniAuth Providers
  supported by GitLab
* GitHost has better performance than other hosted Git solutions because each
  instance only serves one customer, rather than many customers to one
  instance
* You can choose between automatic and manual updates. If you want, your server
  will always be automatically updated to the latest GitLab version, or you can
  stay on your current version and choose when you want the upgrade to happen

### What version of GitLab Enterprise Edition does GitHost support?

GitHost will always install the latest version of GitLab when a new server is
created.

### Are all features of GitLab Enterprise Edition available on GitHost?
{: #githost-unsupported-features}

No, unfortunately some features are not supported on GitHost. Specifically, the following features are **not supported**:

- GitLab Pages
- Kerberos based login
- ChatOps (Mattermost cannot be run in a GitHost instance)
- Integration with ElasticSearch
- Log forwarding
- GitLab Geo
- High Availability
- MySQL database
- Redis Sentinel
- Backup uploading to custom S3
- Custom email configuration
- Grafana
- Reply by email
- Custom `gitlab.rb` file changes
- Setups requiring additonal software installs

### What regions does GitHost operate in?

Amsterdam, London, San Francisco, Singapore, Toronto, New York, Frankfurt, and Bangalore.

### Can I customize my disk space?

Technically, no. Our plans are based on the underlying infrastructure templates.
However, it is possible to attach an external block storage device that allows
you to add up to 16TB of storage for $0.15 USD per GB per Month.

This feature is only available in our NYC1 (New York 1), SFO2 (San Francisco 2), SGP1 (Singapore), and FRA1 (Frankfurt) regions.

### Will I be notified of downtime for maintenance?

Yes.

### Do I need an SSL certificate?

Yes, if you want to use your own domain. You can purchase one from vendors such
as Comodo. No, if you want a `*.githost.io` subdomain.

### Is LDAP supported?

Yes.

### Is SSO supported?

Yes we use OmniAuth so users can sign in using Twitter, Facebook etc.

### Is there support for SAML?

Yes.

### Can I make custom changes to the GitLab configuration?

Custom changes such as increasing the number of Unicorn workers or setting up an
SMTP server for email must be done by a GitLab Service Engineer. Customers can
request this by contacting GitHost Support.

### What connections does GitHost support?

SSH and HTTP.

### How is back-up and recovery managed?

Full backups are taken every day and stored at an offsite location. Restoring
from backups can be done by customers without help from GitHost Support.

### Does GitHost offer privately hosted GitLab CI runners?

Yes, we do. And you can also hook up runners from anywhere.

### Can I migrate from my on-premises GitLab server to GitHost?

No, since GitHost is not longer accepting new customers and will be EOL June 1st 2019 you should not migrate new projects to it.

### Can I migrate my projects from gitlab.com to GitHost?

No, since GitHost is not longer accepting new customers and will be EOL June 1st 2019 you should not migrate new projects to it.

### Can I apply my GitLab Enterprise Edition subscription for use with GitHost if I decide to move from on-premises hosting?

Yes.


### What level of availability does the service provide?

99.9% uptime (also see the [terms](/terms/#githost)).

### What level of support does the service provide?

For assistance with issues that are specific to GitHost, we offer next-business day response times.
Please submit your support request through the [support portal](https://support.gitlab.com/).

### If I subscribe to GitLab Enterprise and use GitHost how do I get support?

GitLab Enterprise subscribers will have the ability to email our Service
Engineers directly for assistance with both GitHost and GitLab, and receive the
benefit of the same response times and service levels that are in place for
GitLab Enterprise Edition subscription.

### How is the service billed?

Subscriptions plans are prorated to the nearest minute and charged monthly.

### Can I combine my annual subscription to GitLab Enterprise Edition and GitHost?

Yes. Please [contact our sales team](/sales).

### Where can I get technical support with my GitHost instance?

Please submit your request for technical support through the [support portal](https://support.gitlab.com).
