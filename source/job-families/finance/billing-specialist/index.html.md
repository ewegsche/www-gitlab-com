---
layout: job_family_page
title: "Billing Specialist"
---

We’re looking for a billing specialist that can manage the invoicing and collection process in its entirety. Specifically, we’re looking for someone to orchestrate and own the customer billing cycle from quote to cash. This role will collaborate with the sales team and at times with customers.  Attention to detail and an aptitude for accuracy are critical to being successful.

We are a rapidly growing company which means you must be comfortable working in fast paced atmosphere where juggling numerous priorities and deadlines is the norm. We are also a fully distributed team which means you must be self-driven and a highly effective communicator.

## Responsibilities

- Completely own and manage the billing process
- Work closely with the Sales team on various billing related issues, including quote generation
- Assist in cross-functional accounting activities as needed
- Resolving customer inquiries in an accurate, timely manner
- Identify process and system improvements to streamline the revenue cycle
- Responsible for owning and administering sales commissions using Xactly
- Communicate process improvements by routinely and frequently updating our handbook and training the sales team.
- Financial reporting and analytics of sales figures and A/R data
- Proactively monitor aging reports, following up on delinquent accounts and managing A/R collections
- Provide sales and billing reports to upper management as needed
- Ability to take on side projects related to internal initiatives

## Requirements

- Proven ability to fully utilize the Zuora platform from quote to cash collection
- Experience with Salesforce CRM
- Experience billing in a high-volume environment
- Deep understanding of subscription billing
- Superior attention to detail
- Excellent computer skills, self starter in picking up new and complex systems
- Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets,etc).
- Slack is a plus but not necessary
- Bonus points: experience with Xactly
- Bonus points: experience using NetSuite
- Bonus points: experience working with distributed teams

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to meet with two other members of the billing team
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
